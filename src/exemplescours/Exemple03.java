package exemplescours;

import formesgeo.Boite;

public class Exemple03 {
    
    public static void main(String[] args) {
           
        Boite uneBoite = new Boite();
        
        System.out.printf("\nPoids total des pièces: %5.2f g\n\n",
        uneBoite.poidsTotal());       
    }
}


