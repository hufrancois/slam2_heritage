package formesgeo;
public abstract class Piece {

  private String repere;
  private String couleur;
  private double densite;

  public Piece( String repere, String couleur, double densite ){
           
    this.repere  = repere;
    this.densite = densite;
    this.couleur = couleur; 
  }
	 
  public abstract double    volume();
        
  public double                 poids(){ return volume()*densite; }
	
  public void                     afficher(){
            
    System.out.printf("REPERE: %-3s  COULEUR: %-8s DENSITE:%6.2f g/cm3\n\n",
            repere, couleur, densite);         
  }
        
  //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
        public String getRepere() {
            return repere;
        }
        
        public void setRepere(String repere) {
            this.repere = repere;
        }
        
        public String getCouleur() {
            return couleur;
        }
        
        public void setCouleur(String couleur) {
            this.couleur = couleur;
        }
        
        public double getDensite() {
            return densite;
        }
        
        public void setDensite(double densite) {
            this.densite = densite;
        }
        
        //</editor-fold>    
}

