package formesgeo;
import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Cone extends Piece {
    
   private double hauteur;
   private double rayon;
  
   public Cone( String repere, String couleur, double densite, double rayon, double hauteur ) {
  
       super(repere,couleur, densite);
       
       this.rayon=rayon;
       this.hauteur=hauteur;
  }

   @Override
   public double volume() { return PI * pow(rayon,2) * hauteur/3;}

   @Override
   public void afficher() {
       
   System.out.print("\nCONE ");
   
   super.afficher();
   
   System.out.printf("Rayon:  %6.2f cm  Hauteur:%6.2f cm\n", rayon , hauteur);
  
   System.out.printf("Volume: %6.2f cm3 Poids:  %6.2f g\n" , volume(), poids());
  }
}
