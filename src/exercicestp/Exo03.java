
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo03 {

    public static void main(String[] args) {
        
        Boite uneBoite = new Boite();
        
        for (Piece p : uneBoite.getLesPieces()) {
        
          if (p.poids()>100 ){
          
              p.afficher();
          }      
        }
        
       
    }
}
