
package exercicestp;

import formesgeo.Sphere;
import formesgeo.Cone;

public class Exo01 {

    public static void main(String[] args) {
        
        Sphere sph1 = new Sphere("S", "rouge", 2.70 , 3.50);
        Sphere sph2 = new Sphere("T", "rouge", 4.43 , 2);
        Cone con1 = new Cone("U", "vert", 4.43 , 2, 3.50);
        
        sph1.afficher();
        sph2.afficher();
        con1.afficher();
        
        System.out.println();
        
        
    }
}
