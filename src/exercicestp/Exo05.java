
package exercicestp;

import formesgeo.Boite;
import formesgeo.Sphere;
import formesgeo.Piece;

public class Exo05 {

    public static void main(String[] args) {
        
        Boite uneBoite = new Boite();
        
        for (Piece p : uneBoite.getLesPieces()) {
        
          if (p instanceof Sphere && p.getCouleur() == "rouge" ){
          
              p.afficher();
          }      
        }
        
    }
}
