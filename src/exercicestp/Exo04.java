
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo04 {

    public static void main(String[] args) {

        Boite uneBoite = new Boite();
        double poids = 0;
        
        for (Piece p : uneBoite.getLesPieces()) {
        
          if (p.getCouleur() == "rouge" ){
              
              p.afficher();
              poids += p.poids();
              
          }      
        }
        System.out.println("");
        System.out.printf("Le poids total des pièce rouge est de : %5.2f g\n\n",poids);
    }
}
