
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo06 {

    public static void main(String[] args) {
        
        Boite uneBoite = new Boite();
        double poidsMini = uneBoite.getLesPieces().get(0).poids();
        Piece piece = null;
        for (Piece p : uneBoite.getLesPieces()) {
          if (p.poids() <= poidsMini){
              piece = p;
              poidsMini = p.poids();
          }      
        }
        piece.afficher();
        
    }
}
